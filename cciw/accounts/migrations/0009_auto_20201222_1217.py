# Generated by Django 3.0.6 on 2020-12-22 12:17

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("accounts", "0008_role_allow_emails_from_public"),
    ]

    operations = [
        migrations.AlterField(
            model_name="role",
            name="email_recipients",
            field=models.ManyToManyField(
                blank=True,
                help_text='Users who will be emailed for email sent to the role email address. Usually the same as "members", or a subset',
                related_name="roles_as_email_recipient",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
    ]
